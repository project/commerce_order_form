<?php
/**
 * @file
 * Views Style plugin extension.
 */

class commerce_order_form_style extends views_plugin_style_table {
  /**
   * {@inheritdoc}
   */
  function pre_render($result) {
    $product_ids = array();

    foreach ($result as $result_item) {
      $product_ids[] = $result_item->product_id;
    }

    $order_form = drupal_get_form('commerce_order_form_product_form', $product_ids);

    $this->view->order_form = $order_form;
  }

  /**
   * {@inheritdoc}
   */
  function render() {
    $table_output = parent::render();

    $order_form = $this->view->order_form;

    $order_form['products']['#printed'] = TRUE;
    $order_form['view']['#markup'] = $table_output;

    return render($order_form);
  }
}