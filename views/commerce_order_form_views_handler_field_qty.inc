<?php

/**
 * Defines the quantity field for the commerce order form view.
 */
class commerce_order_form_views_handler_field_qty extends views_handler_field {
  /**
   * {@inheritdoc}
   *
   * Perform any database or cache data retrieval here. In this example there is
   * none.
   */
  function query() {

  }

  /**
   * {@inheritdoc}
   *
   * Modify any end user views settings here. Debug $options to view the field
   * settings you can change.
   */
  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  /**
   * {@inheritdoc}
   *
   * Make changes to the field settings form seen by the end user when adding
   * your field.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  function validate() {
    $errors = array();

    $view = $this->view;
    $display_style = $view->display_handler->get_option('style_plugin');

    if ($display_style !== 'commerce_order_form_style') {
      foreach ($view->display_handler->handlers['field'] as $view_field) {
        if (is_a($view_field, 'commerce_order_form_views_handler_field_qty')) {
          $errors['display_incorrect'] = t('Display Format must be set to Commerce Order Form in order to use Quantity field.');
        }
      }
    }

    return $errors;
  }

  /**
   * Render callback handler.
   *
   * Return the markup that will appear in the rendered field.
   */
  function render($values) {
    $output = NULL;
    $view = $this->view;

    // Order form attribute on view is set by commerce_ordre_form_style handler.
    if (empty($view->order_form)) {
      drupal_set_message(t('View display is not order form type.'), 'error');
    }
    else {
      $product_id = $values->product_id;
      $order_form = $view->order_form;

      $output = drupal_render($order_form['products'][$product_id]);
    }

    return $output;
  }
}
