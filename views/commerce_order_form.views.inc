<?php
/**
 * @file
 * Views integration for Commerce Order Form.
 */

/**
 * Implements hook_views_plugins().
 */
function commerce_order_form_views_plugins() {
  $path = drupal_get_path('module', 'commerce_order_form');
  $plugins = array(
    'style' => array(
      'commerce_order_form_style' => array(
        'title' => t('Commerce Order Form'),
        'help' => t('Gives end users the ability to add a selection of products from the view to their cart'),
//        'help topic' => 'style-ip-geoloc-map',
        'handler' => 'commerce_order_form_style',
        'path' => $path . '/views',
        'theme' => 'views_view_table',
//        'theme path' => $path . '/views',
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'uses grouping' => TRUE,
        'uses row plugin' => FALSE,
        'type' => 'normal',
        'even empty' => TRUE,
      ),
    ),
  );

  return $plugins;
}

/**
 * Implements hook_views_data().
 */
function commerce_order_form_views_data() {
  $data['commerce_order_form']['table']['group'] = t('Commerce Order Form');
  $data['commerce_order_form']['table']['join'] = array(
//    // Exist in all views.
//    '#global' => array(),
    'commerce_product' => array(),
  );

  $data['commerce_order_form']['commerce_order_form_qty'] = array(
    'title' => t('Quantity'),
    'help' => t('The quantity to purchase.'),
    'field' => array(
      'handler' => 'commerce_order_form_views_handler_field_qty',
    ),
  );

  return $data;
}